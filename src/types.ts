// We only use certain field.
// export interface OpenWeatherApiResponse {
//   coord: {
//     lon: number,
//     lat: number
//   },
//   weather: [
//     {
//       id: number,
//       main: string,
//       description: string,
//       icon: string
//     }
//   ],
//   base: string,
//   main: {
//     temp: number | string,
//     feels_like: number,
//     temp_min: number,
//     temp_max: number,
//     pressure: number,
//     humidity: number
//   },
//   visibility: number,
//   wind: {
//     speed: number,
//     deg: number
//   },
//   clouds: {
//     all: number
//   },
//   dt: number,
//   sys: {
//     type: number,
//     id: number,
//     message: number,
//     country: string,
//     sunrise: number,
//     sunset: number
//   },
//   timezone: number,
//   id: number,
//   name: string,
//   cod: number
// }

// export interface OpenWeatherForecastApiResponse {
//   cod: string,
//   message: number,
//   cnt: number,
//   list: {
//     dt: number,
//     main: {
//       temp: number,
//       feels_like: number,
//       temp_min: number,
//       temp_max: number,
//       pressure: number,
//       sea_level: number,
//       grnd_level: number,
//       humidity: number,
//       temp_kf: number
//     },
//     weather: [
//       {
//         id: number,
//         main: string,
//         description: string,
//         icon: string
//       }
//     ],
//     clouds: {
//       all: number
//     },
//     wind: {
//       speed: number,
//       deg: number
//     },
//     sys: {
//       pod: string
//     },
//     dt_txt: string
//   }[],
//   city: {
//     id: number,
//     name: string,
//     coord: {
//       lat: number,
//       lon: number
//     },
//     country: string,
//     timezone: number,
//     sunrise: number,
//     sunset: number
//   }
// }

export interface OpenWeatherForecastApiResponse {
  list: {
    dt: number,
    main: {
      temp: number | string,
    },
    weather: [
      {
        main: string,
        icon: string
      }
    ],
  }[],
  city: {
    timezone: number,
  }
}

export interface OpenWeatherApiResponse {
  main: {
    temp: number | string,
  },
  weather: [
    {
      main: string,
      icon: string
    }
  ],
  name: string | undefined,
  dt: number,
  timezone: number
}