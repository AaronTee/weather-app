import React, { FunctionComponent, useMemo } from "react";
import { WeatherCardProps, WeatherCard } from "./WeatherCard";
import styled from 'styled-components';

interface WeatherCardListProps {
  className?: string,
  weatherCards: WeatherCardProps[],
  onWeatherCardRemoved?: (index: number) => void,
  onWeatherCardError?: (err: Error, index: number) => void
}

export const WeatherCardList = styled<FunctionComponent<WeatherCardListProps>>((props) => {
  
  const { weatherCards, onWeatherCardRemoved, onWeatherCardError } = props;

  const renderWeatherCards = useMemo(() => {
    return weatherCards.map((wcProps, i) => (
      <WeatherCard
        key={wcProps.cityName}
        onRemoveClicked={() => onWeatherCardRemoved && onWeatherCardRemoved(i)} 
        onLoadError={(err: Error) => onWeatherCardError && onWeatherCardError(err, i)}
        {...wcProps} 
      />
    ));
  }, [weatherCards, onWeatherCardRemoved, onWeatherCardError]);
  
  return (
    <div className={props.className}>
      { renderWeatherCards }
    </div>
  )
})`  
  // Fallback if browser doesn't support
  display: flex;

  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(20rem, 1fr));
  grid-auto-rows: 1fr;
  grid-gap: 25px;
  justify-content: space-between;

  &::before {
    content: '';
    width: 0;
    padding-bottom: 100%;
    grid-row: 1 / 1;
    grid-column: 1 / 1;
  }

  & > ${WeatherCard}:first-child {
    grid-row: 1 / 1;
    grid-column: 1 / 1;
  }

  & ${WeatherCard} {
    width: 100%;
    height: 100%;
  }
`