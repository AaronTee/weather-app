import styled, { css } from "styled-components";

export interface FlexWrapperProps<T extends HTMLElement> extends React.HTMLAttributes<T> {
  direction?: 'row' | 'column',
  flex?: string,
  flexWrap?: 'wrap' | 'nowrap' | 'unset',
  alignItem?: 'stretch' | 'flex-start' | 'flex-end' | 'center' | 'baseline',
  justifyContent?: 'space-between' | 'flex-start' | 'flex-end' | 'center'
}

export const FlexWrapperCss = css<FlexWrapperProps<HTMLDivElement>>`
  display: flex;
  flex-direction: ${(props) => props.direction};
  flex: ${(props) => props.flex};
  flex-wrap: ${(props) => props.flexWrap};
  justify-content:${(props) => props.justifyContent};
  align-items: ${(props) => props.alignItem};
`;

const FlexWrapper = styled.div.attrs(props => ({
  style: props.style
}))<FlexWrapperProps<HTMLDivElement>>`
  ${FlexWrapperCss}
`;

FlexWrapper.defaultProps = {
  direction: 'row',
  flexWrap: 'wrap',
  alignItem: 'stretch',
  justifyContent: 'space-between',
  flex: undefined
}

export {
  FlexWrapper
};