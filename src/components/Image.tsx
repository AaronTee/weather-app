import React, { FunctionComponent, ImgHTMLAttributes } from 'react';
import styled from "styled-components";

interface ImageProps extends ImgHTMLAttributes<HTMLImageElement> {
  width?: number,
  height?: number
}

export const Image = styled<FunctionComponent<ImageProps>>((props) => {
  return <img alt={props.alt || 'image'} {...props} />
})`
  display: block;
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center center;
  width: ${(props => !!props.width ? `${props.width}px` : '100%')};
  height: ${(props => !!props.height ? `${props.height}px` : '')};
`;