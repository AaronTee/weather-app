import styled from "styled-components"
import React, { useState } from "react";
import { FlexWrapperProps, FlexWrapperCss } from "../Layout/Wrapper";

interface FormProps<D extends {}> extends FlexWrapperProps<HTMLFormElement> {
  name?: string,
  onFormSubmit?: (e: React.FormEvent<HTMLFormElement>, formData: D) => void
}

const Form = styled<React.FunctionComponent<FormProps<{}>>>((props) => {
  
  const [formData, setFormData] = useState<{ [key: string]: string }>({});
  
  const onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    props.onFormSubmit && props.onFormSubmit(e, formData);
    e.preventDefault();
  }

  const onChange = (e: React.FormEvent<HTMLFormElement>) => {
    const el = e.target as HTMLInputElement | HTMLSelectElement;
    switch (el.tagName.toLowerCase()) {
      case 'input':
      case 'select':
        setFormData({
          ...formData,
          [el.name]: el.value
        })
        break;
    }
  }

  return (
    <form
      onSubmit={onSubmit}
      onChange={onChange}
      className={`${props.className}`}
    >
      {props.children}
    </form>
  );
})`
  ${FlexWrapperCss}
`

Form.defaultProps = {
  direction: 'column',
};

export { Form };
