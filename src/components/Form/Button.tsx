
import React, { FunctionComponent } from 'react';
import styled from "styled-components";

interface ButtonProps {
  label: string,
  name?: string,
  type?: "button" | "submit" | "reset" | undefined,
  className?: string,
  onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void
}

const Button = styled<FunctionComponent<ButtonProps>>((props) => {
  return (
    <button type={props.type} name={props.name} onClick={props.onClick} className={props.className}>
      {props.label}
    </button>
  )
})`
`

Button.defaultProps = {
  label: 'Click',
  type: 'button'
}

export { Button }
