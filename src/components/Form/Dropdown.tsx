import React, { useState, useEffect } from 'react';
import styled from "styled-components";
import { FunctionComponent } from "react";

export interface DropdownProps {
  name?: string,
  options: DropdownOptionProps[],
  onChange?: (e: React.ChangeEvent<HTMLSelectElement>) => void
}

export interface DropdownOptionProps {
  label: string,
  value: string | number | undefined
}

export const Dropdown = styled<FunctionComponent<DropdownProps>>((props) => {

  const [value, setValue] = useState<string>('');
  const [options, setOptions] = useState<DropdownOptionProps[]>(props.options || []);

  useEffect(() => {
    setOptions(props.options);
  }, [props.options]);

  const onChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    props.onChange && props.onChange(e);
    setValue(e.target.value);
  };

  return (
    <select name={props.name} onChange={onChange} value={value}>
      { options.map((o, i) => <option key={o.value + i.toString()} label={o.label} value={o.value}></option>) }
    </select>
  );
})`
`
