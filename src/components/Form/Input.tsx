import React, { useState } from 'react';
import styled from "styled-components";
import { FunctionComponent } from "react";

interface InputProps {
  className?: string,
  name?: string,
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void,
  placeholder?: string,
  width?: number
}

const Input = styled<FunctionComponent<InputProps>>((props) => {

  const [value, setValue] = useState<string>('');

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    props.onChange && props.onChange(e);
    setValue(e.target.value);
  };

  return (
    <input className={props.className} name={props.name} onChange={onChange} placeholder={props.placeholder} value={value} />
  )
})`
  min-width: ${props => props.width}px;
`

Input.defaultProps = {
  width: 200
}

export { Input }