import React, { FunctionComponent, useState, useEffect, useMemo } from "react";
import axios from 'axios';
import { OpenWeatherApiResponse, OpenWeatherForecastApiResponse } from "../types";
import styled from "styled-components";
import { Image } from "./Image";
import config from '../config';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import { FlexWrapper } from "./Layout/Wrapper";

export interface WeatherCardProps {
  className?: string,
  cityName: string,
  refreshIntervalMs: number | undefined,
  onRemoveClicked?: () => void,
  onLoadError?: (err: Error) => void
}

dayjs.extend(utc);

export const WeatherCard = styled<FunctionComponent<WeatherCardProps>>((props) => {

  const { onRemoveClicked, refreshIntervalMs, cityName, onLoadError, className } = props;

  const dayNightIconNames = ['01d', '01n', '02d', '02n'];
  
  let cancelTokenSource = axios.CancelToken.source();
  const [ openWeather, setOpenWeather ] = useState<OpenWeatherApiResponse>({
    name: undefined,
    weather: [{
      main: '-',
      icon: '',
    }],
    main: {
      temp: '-'
    },
    dt: 0,
    timezone: 0
  });

  const [ openWeatherForecast, setOpenWeatherForecast ] = useState<OpenWeatherForecastApiResponse>();
  const [ isFetchingWeather, toggleIsFetchingWeather ] = useState<boolean>(false);

  useEffect(() => {
    if (!refreshIntervalMs) 
      return;

    setInterval(() => fetchWeather(cityName), refreshIntervalMs);
  }, [refreshIntervalMs])

  const fetchWeather = async(cityName: string) => {

    let error;
    toggleIsFetchingWeather(true);

    try {
      let respCurrent = await axios.get<OpenWeatherApiResponse>(
        `https://api.openweathermap.org/data/2.5/weather?q=${cityName}&units=metric&appid=${config.openWeatherAppId}`, {
          cancelToken: cancelTokenSource.token
        }
      );

      let respForecast = await axios.get<OpenWeatherForecastApiResponse>(
        `http://api.openweathermap.org/data/2.5/forecast?q=${cityName}&units=metric&appid=${config.openWeatherAppId}`, {
          cancelToken: cancelTokenSource.token
        }
      );

      setOpenWeather(respCurrent.data);
      setOpenWeatherForecast(respForecast.data);

    } catch(err) {
      if (axios.isCancel(err))
        return;
      error = err;
    } finally {
      toggleIsFetchingWeather(false);
    }

    // Avoid setState after component unmounted.
    error && !openWeather.name && onLoadError && onLoadError(error);
  }

  // For console warning 'react-hooks/exhaustive-deps' https://github.com/facebook/create-react-app/issues/6880
  useEffect(() => {
    fetchWeather(cityName);

    return () => {
      cancelTokenSource.cancel("Component unmounted.");
    }
  }, [])

  const renderForecastWeathers = useMemo(() => {

    if (!openWeatherForecast)
      return (null);

    const { timezone } = openWeatherForecast.city;

    return openWeatherForecast.list.filter((_, i) => i !== 0 && i % 8 === 0).map(owf => {

      const { weather: weatherArr, main, dt } = owf;
      
      const weather = weatherArr[0];
      let weatherIconName = weather.icon;

      if (dayNightIconNames.indexOf(owf.weather[0].icon) < 0)
        weatherIconName = owf.weather[0].icon.slice(0,-1);

      return (
        <div className={'weather-forecast-item'}>
          <span>{dayjs((dt + timezone) * 1000).utc().format('D/M')}</span>
          <Image
            src={`/weather-icon/${weatherIconName}.svg`}
            className="weather-widget__image"
          />
        </div>
      )})
  }, [openWeatherForecast]);
  
  const { weather: weatherArr, main, dt, timezone } = openWeather;
  const weather = weatherArr[0];
  let weatherIconName = weather.icon;
  
  if (dayNightIconNames.indexOf(weather.icon) < 0)
  weatherIconName = weather.icon.slice(0,-1)

  if (!openWeather.name) 
    return (null)

  return (
    <div className={`${className} weather-list-item`}>
      <div className="close-widget">
        <Image
          src={'/cancel.svg'}
          className='close-widget-image'
          onClick={onRemoveClicked}
          width={50}
        />
      </div>
      <div className="weather-header">
        <h3>{openWeather.name || '-'}</h3>
        <FlexWrapper direction={'row'}>
          <small>as of {dayjs((dt + timezone) * 1000).utc().format('h.mma, D MMM')}</small>
          <FlexWrapper direction={'row'}>
            { !!refreshIntervalMs && <Image src={'/loading.svg'} width={15} height={15}/> }
            { isFetchingWeather && <Image className={'weather-header__spinner'} src={'/refresh.svg'} width={15} height={15}/> }
          </FlexWrapper>
        </FlexWrapper>
      </div>
      <div className="weather-widget">
        <Image
          src={`/weather-icon/${weatherIconName}.svg`}
          className="weather-widget__image"
        />
      </div>
      <div className="weather-footer">
        <div>{(+main.temp).toFixed(1)} C°</div>
        <div>{weather.main}</div>
      </div>
      <h5>Forecast</h5>
      <FlexWrapper direction={'row'} className={'weather-forecast-container'}>
        { renderForecastWeathers }
      </FlexWrapper>
      
      
    </div>
  )
})`

  // Can be moved to mixin
  @keyframes spin {
    from {transform:rotate(0deg);}
    to {transform:rotate(360deg);}
  }

  @keyframes fade-in-from-right {
    from {
      transform:translateX(50px);
      opacity: 0;
    }
    to {
      transform:translateX(0px);
      opacity: 1;
    }
  }

  position: relative;
  animation: fade-in-from-right .6s ease-out forwards;

  &:hover {
    & .close-widget {
      background: rgba(255, 255, 255, .7);

      & .close-widget-image {
        visibility: visible;
      }
    }
  }

  .weather-header {
    & .weather-header__spinner {
      animation: spin 1s infinite ease-out;
    }
  }

  .weather-forecast-container {
    & > .weather-forecast-item {
      flex: 1 0 25%;
    }
  }

  .weather-footer {
    display: flex;
    justify-content: space-between;
    margin-top: 6px;
  }

  .weather-widget {
    .weather-widget__image {
      background-color: #f7f7f7;
    }
  }

  .close-widget {
    display: flex;
    justify-content: center;
    align-items: center;
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background: rgba(255, 255, 255, 0);
    transition: background .3s ease-out;

    & .close-widget-image {
      transition: opacity .2s ease-out;
      opacity: .7;
      visibility: hidden;
      cursor: pointer;

      &:hover {
        opacity: 1;
      }
    }
  }
`