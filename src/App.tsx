import React, { useState, FunctionComponent } from 'react';
import './App.css';
import { WeatherCardList } from './components/WeatherCardList';
import { WeatherCardProps } from './components/WeatherCard';
import styled from 'styled-components';
import { Dropdown, DropdownOptionProps } from './components/Form/Dropdown';
import { Button } from './components/Form/Button';
import { Input } from './components/Form/Input';
import { Form } from './components/Form';

interface AppProps {
  className?: string
}

const App = styled<FunctionComponent<AppProps>>((props) => {

  const [ citiesWeather, setCitiesWeather ] = useState<WeatherCardProps[]>([{
    cityName: 'Kuala Lumpur',
    refreshIntervalMs: 60000,
  },
  {
    cityName: 'Singapore',
    refreshIntervalMs: 60000,
  }]);
  
  const [ errorMessage, setErrorMessage ] = useState<string>('');
  const refreshIntervalOptions: DropdownOptionProps[] = [
    {
      label: 'Do not refresh',
      value: undefined,
    },
    {
      label: '15 Seconds',
      value: 10000,
    },
    {
      label: '1 Minute',
      value: 60000,
    },
    {
      label: '15 Minute',
      value: 900000,
    }
  ]

  const onFormSubmit = (e: React.FormEvent<HTMLFormElement>, formData: any) => {
    setErrorMessage('');

    if (!formData.city)
    setErrorMessage('City must not leave blank');
    
    if (citiesWeather.map(cw => cw.cityName).indexOf(formData.city) >= 0) {
      setErrorMessage('Already in the list');
      return;
    }

    setCitiesWeather(cities => [
      ...cities,
      { 
        cityName: formData.city,
        refreshIntervalMs: formData.interval
      }
    ])
  }

  const onWeatherCardRemoved = (index: number) => {
    setCitiesWeather(cur => cur.filter((_, i) => i !== index));
  }

  const onWeatherCardLoadError = (err: any, index: number) => {

    switch (err.response.status) {
      case 404:
        setErrorMessage('City/Country is not exist in this weather app.');
        break;

      case 429:
        setErrorMessage(err.response.data.message + '. For tester, this weather app uses OpenWeatherMap api, please configure your own appId in src/config.ts file.');
        break;
    }

    onWeatherCardRemoved(index);
  }

  return (
    <div className={props.className}>
      <h1>Weather App</h1>
      <WeatherCardList
        className={'weather-card-list'}
        weatherCards={citiesWeather}
        onWeatherCardRemoved={onWeatherCardRemoved}
        onWeatherCardError={onWeatherCardLoadError}
      />
      <div className={'spacer'} />
      <div className={'form-area'}>
        <h2>Enter City</h2>
        <Form direction={'row'} justifyContent={'center'} onFormSubmit={onFormSubmit}>
          <Input placeholder={'e.g. Singapore, Dublin'} name={'city'} />
          <Dropdown name={'interval'} options={refreshIntervalOptions}/>
          <Button type={'submit'} label={'Add'} />
        </Form>
        <p className={'form-error-message'}>{ errorMessage }</p>
      </div>
    </div>
  );
})`
// styled highlighting issue: https://github.com/styled-components/vscode-styled-components/issues/201
  max-width: 1280px;
  margin: 0 auto;
  padding: 50px;

  .form-area {

    & > * {
      text-align: center;
    }

    width: 100%;
    margin: 0 auto;
  }

  .spacer {
    padding-top: 48px;
    padding-bottom: 8px;
  }

  .form-error-message {
    color: red;
  }
`;

export default App;
